CREATE TABLE users (
      id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
      name varchar(127) NOT NULL,
      nickname varchar(32) NOT NULL,
      description TEXT,
      password varchar(255) NOT NULL,
      FULLTEXT(name, nickname)
);

CREATE TABLE statuses (
      id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
      text varchar(255) NOT NULL,
      timestamp timestamp NOT NULL,
      author INTEGER NOT NULL,
      FOREIGN KEY (author) REFERENCES users(id) ON DELETE CASCADE
);
CREATE INDEX statuses_timestamp ON status(timestamp);

CREATE TABLE following (
    stalker_id INTEGER NOT NULL,
    followee_id INTEGER NOT NULL,
    FOREIGN KEY (stalker_id) REFERENCES users(id) ON DELETE CASCADE,
    FOREIGN KEY (followee_id) REFERENCES users(id) ON DELETE CASCADE,
    UNIQUE following_pair (stalker_id, followee_id)
);
