package pt.dcruz.tweets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pt.dcruz.tweets.model.Tweet;
import pt.dcruz.tweets.model.User;
import pt.dcruz.tweets.model.service.TweetManager;

/**
 * Handles requests for the application home page.
 */
@RequestMapping @Controller
public class TimelineController {
	
	private static final Logger logger = LoggerFactory.getLogger(TimelineController.class);
	private TweetManager tweetManager;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model, HttpSession session) {
		
		if (session.getAttribute("loggedUser") != null) {
			User user = (User)session.getAttribute("loggedUser");
			model.addAttribute("tweets", tweetManager.getUserTweetWithFollow(user.getId(), 20));
		} else {
			model.addAttribute("tweets", tweetManager.getTweets());
		}
		
		return "home";
	}
	
	@RequestMapping(value = "/timeline", method = RequestMethod.GET)
	public String getFullTimeline(@RequestParam("id") int user, Model model) {
		model.addAttribute("tweets", tweetManager.getUserTweetList(user));
		
		return "tweetlist";
	}	
	
	@RequestMapping(value = "/extendTimeline", method = RequestMethod.GET)
	public String getExtendedTimeline(HttpServletRequest request, Model model) {
		int user = 0;
		int howMany = 20;
		
			String idParam = request.getParameter("id");
			String countParam = request.getParameter("count");
			
			if (idParam != null && idParam.length() >= 1) {
				user = Integer.parseInt(idParam);
			} else {
				user = 0;
			}
			
			if (countParam != null && countParam.length() >= 1) {
				howMany = Integer.parseInt(countParam);
			} else {
				howMany = 20;
			}

			if (howMany <= 0) {
				howMany = 20;
			}
			
			if( user > 0) {
				model.addAttribute("tweets", tweetManager.getUserTweetWithFollow(user, howMany));
			} else {
				model.addAttribute("tweets", tweetManager.getTweets(howMany));
			}
		
		return "home";
	}
	
	@RequestMapping(value = "/newer", method = RequestMethod.GET)
	public String getNewerTweets(@RequestParam("id") int newerThan, Model model) {
		
		model.addAttribute("tweets",tweetManager.getNewerTweets(newerThan));
		
		return "tweetlist";
	}
	
	@RequestMapping(value = "/more")
	public String getMoreTweets(@RequestParam("id") int olderThan, Model model) {
		
		model.addAttribute("tweets",tweetManager.getOlderTweets(olderThan, 10));

		return "tweetlist";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String add(@RequestParam("text") String text, Model model, HttpSession session) {
		
		if (session.getAttribute("loggedUser") != null) {
			logger.info("Logged user: "+ session.getAttribute("loggedUser") +" added a tweet");
			User user = (User)session.getAttribute("loggedUser");
			
			Tweet tweet = new Tweet();
			tweet.setText(text);
			tweet.setAuthorId(user.getId());
			
			tweetManager.saveTweet(tweet);
		} else {
			logger.info("Not logged");
		}
		
		return "redirect:/";
	}

	public TweetManager getTweetManager() {
		return tweetManager;
	}

	public void setTweetManager(TweetManager tweetManager) {
		this.tweetManager = tweetManager;
	}
	
}