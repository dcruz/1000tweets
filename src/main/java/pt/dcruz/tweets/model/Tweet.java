package pt.dcruz.tweets.model;

import java.io.Serializable;
import java.util.Date;

public class Tweet implements Serializable, Comparable<Tweet> {

	private int id;
	private String text;
	private Date timestamp;
	private int authorId;
	private String name;
	private String username;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("Tweet text: " + text + ";");
		buffer.append("Tweet timestamp: " + timestamp);
		return buffer.toString();
	}

	public int getAuthorId() {
		return authorId;
	}

	public void setAuthorId(int authorId) {
		this.authorId = authorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int compareTo(Tweet arg0) {
		return timestamp.compareTo(arg0.getTimestamp()) * (-1);
	}
}