package pt.dcruz.tweets.model.service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import pt.dcruz.tweets.dao.TweetDao;
import pt.dcruz.tweets.model.Tweet;

public class SimpleTweetManager implements TweetManager {

    private TweetDao tweetDao;

    public List<Tweet> getTweets() {
        return tweetDao.getTweetList();
    }
    
    public List<Tweet> getTweets(int howMany) {
        return tweetDao.getTweetList(howMany);
    }
    
	public List<Tweet> getUserTweetList(int user) {
		return tweetDao.getUserTweetList(user);
	}

	public List<Tweet> getUserTweetList(int user, int howMany) {
		return tweetDao.getUserTweetList(user, howMany);
	}
    
	public List<Tweet> getUserTweetWithFollow(int user, int howMany) {
		List<Tweet> personalTweets = tweetDao.getUserTweetList(user, howMany);
		List<Tweet> followTweets = tweetDao.getFollowTweetList(user, howMany);
		
		personalTweets.addAll(followTweets);
		Collections.sort(personalTweets);
		
		if (personalTweets.size() < howMany) {
			howMany = personalTweets.size();
		}
		
		return personalTweets.subList(0, howMany);
	}
	
    public List<Tweet> getNewerTweets(int id) {
    	return tweetDao.getNewerTweets(id);
    }
    
    public List<Tweet> getOlderTweets(int id, int howMany) {
    	return tweetDao.getOlderTweet(id, howMany);
    }

    public void saveTweet(Tweet tweet) {
        tweetDao.saveTweet(tweet);
    }

    // Getters and Setters
    public void setTweetDao(TweetDao tweetDao) {
        this.tweetDao = tweetDao;
    }
}

