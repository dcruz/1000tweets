package pt.dcruz.tweets.model.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pt.dcruz.tweets.TimelineController;
import pt.dcruz.tweets.dao.UserDao;
import pt.dcruz.tweets.model.User;

public class SimpleUserManager implements UserManager {

	private static final Logger logger = LoggerFactory.getLogger(TimelineController.class);
	
    private UserDao userDao;
    
	public List<User> getUserList() {
		return this.userDao.getUserList();
	}

	public User getUser(int id){
		return this.userDao.getUser(id);
	}
	
	public User getUser(String username) {
		return this.userDao.getUser(username);
	}
	
	public boolean userExists(String username) {
		return this.userDao.getUser(username) != null;
	}
	
	public List<User> searchUsers(String query) {
		return userDao.searchUsers(query);
	}

	/**
	 * 
	 */
	public int addNewUser(User user) {
		return userDao.saveUser(user);
	}
	
	public boolean isPasswordValid(int id, String password) {
		String savedPassword = userDao.getUserPassword(id);
		
		//TODO: Use a hashed password instead
		
		return savedPassword.equals(password);
	}

	public List<Integer> getFollowingList(int user) {
		return userDao.getFollowingList(user);
	}

	public void follow(int follower, int followee) {
		this.userDao.follow(follower, followee);
	}

	public void unfollow(int follower, int followee) {
		this.userDao.unfollow(follower, followee);
	}
	
    // Getters and Setters
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}