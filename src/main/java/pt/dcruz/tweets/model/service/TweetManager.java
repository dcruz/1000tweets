package pt.dcruz.tweets.model.service;

import java.io.Serializable;
import java.util.List;

import pt.dcruz.tweets.model.Tweet;

public interface TweetManager extends Serializable{
    
    public List<Tweet> getTweets();
    
    public List<Tweet> getTweets(int howMany);
    
    public List<Tweet> getUserTweetList(int user);
    
    public List<Tweet> getUserTweetList(int user, int howMany);
    
    public List<Tweet> getUserTweetWithFollow(int user, int howMany);
    
    public List<Tweet> getNewerTweets(int id);
    
    public List<Tweet> getOlderTweets(int id, int howMany);

    public void saveTweet(Tweet tweet);
    
}

