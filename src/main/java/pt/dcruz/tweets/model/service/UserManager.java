package pt.dcruz.tweets.model.service;

import java.io.Serializable;
import java.util.List;

import pt.dcruz.tweets.model.Tweet;
import pt.dcruz.tweets.model.User;

public interface UserManager extends Serializable{
	
	public List<User> getUserList();
    
	public User getUser(int id);
	
	public User getUser(String username);
	
	public boolean userExists(String username);
	
    public List<User> searchUsers(String query);

    /**
     * Add a new user
     * @param user
     * @return the ID of the new user
     */
    public int addNewUser(User user);
    
    public boolean isPasswordValid(int id, String password);

    public List<Integer> getFollowingList(int user);
    
    public void follow(int follower, int followee);

    public void unfollow(int follower, int followee);
}