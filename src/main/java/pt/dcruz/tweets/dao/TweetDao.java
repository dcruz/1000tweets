package pt.dcruz.tweets.dao;

import java.util.List;

import pt.dcruz.tweets.model.Tweet;

public interface TweetDao {

    public List<Tweet> getTweetList();
    
    public List<Tweet> getTweetList(int howMany);
    
    public List<Tweet> getUserTweetList(int user);
    
    public List<Tweet> getFollowTweetList(int user, int howMany);
    
    public List<Tweet> getUserTweetList(int user, int howMany);

    public List<Tweet> getNewerTweets(int newerThan);
    
    public List<Tweet> getOlderTweet(int id, int howMany);
    
    public void saveTweet(Tweet tweet);

}

