package pt.dcruz.tweets.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import pt.dcruz.tweets.model.User;

public class JdbcUserDao extends SimpleJdbcDaoSupport implements UserDao {

	/** Logger for this class and subclasses */
	private static final Logger logger = LoggerFactory.getLogger(JdbcUserDao.class);
	
	public List<User> getUserList() {
	        logger.info("Requesting users");
	        
	        String selectUsers = "SELECT id, name, username FROM users";
	        
	        List<User> users = getJdbcTemplate().query(selectUsers,
	        		new UserMapper());
	        
	        logger.info("Returning "+ users.size() +" users");
	        
	        return users;
	}
	
	public List<User> searchUsers(String query) {
        logger.info("Searching users with query: "+ query);
        
        String selectUsers = "SELECT id, name, nickname, description FROM users"
        		+" WHERE MATCH(name, nickname) AGAINST(? WITH QUERY EXPANSION)";
         
        List<User> users = getJdbcTemplate().query(selectUsers,
        		new UserMapper(),
        		new Object[]{query});
        
        logger.info("Found "+ users.size() +" users for the query: "+ query);
        
        return users;
	}
	
	public User getUser(int id) {
        logger.info("Requesting user wit id: "+ id);
        
        String selectUserById = "SELECT id, name, nickname, description FROM users"
        		+" WHERE id = ?";
        
        User user = getJdbcTemplate().queryForObject(selectUserById,
        		new UserMapper(),
        		new Object[]{id});
        
        logger.info("Found "+ user.getUsername() +", users with id: "+ user.getId());
        
        return user;
	}
	
	public User getUser(String username) {
        logger.info("Requesting user with username: "+ username);
        
        String selectUserByUsername = "SELECT id, name, nickname, description FROM users"
        		+" WHERE nickname = ?";
        
        User user = null;
        
        try {
        	user = getJdbcTemplate().queryForObject(selectUserByUsername,
        			new UserMapper(),
        			new Object[]{username});
        
        	logger.info("Found "+ user.getUsername() +", users with id: "+ user.getId());
        } catch(Exception e) {
        	logger.info("User with username "+ username +" wasn't found.");
        }
        
        return user;
	}
	
	

	public int saveUser(User user) {
        logger.info("Saving new user: "+ user.getUsername());
        
        String insertNewUser = "INSERT INTO users(name, nickname, password, description)"
        		+" VALUES (:name, :username, :password, :description)";
        
        int count = getSimpleJdbcTemplate().update(insertNewUser,
            new MapSqlParameterSource().addValue("username", user.getUsername())
                .addValue("name", user.getName())
                .addValue("password", user.getPassword())
                .addValue("description", user.getDescription()));
        
        return getUser(user.getUsername()).getId();
	}
	
	public List<Integer> getFollowingList(int user) {
        logger.info("Getting the ID list of all users followed by: "+ user);
        
        String selectFollowingById = "SELECT followee_id FROM following"
        		+" WHERE stalker_id = ?";
        
        List<Integer> usersFollowed = getJdbcTemplate().query(selectFollowingById,
        		new FollowingMapper(),
        		new Object[]{user});
        
        logger.info("Found "+ usersFollowed +", users followed id: "+ user);
        
        return usersFollowed;
	}
	
	public String getUserPassword(int id) {
        logger.info("Requesting user password with id: "+ id);
        
        String selectUserByUsername = "SELECT password FROM users"
        		+" WHERE id = ?";
        
        String password = getJdbcTemplate().queryForObject(selectUserByUsername,
        		new PasswordMapper(),
        		new Object[]{id});
        
        logger.info("Found password for users with id: "+ id);
        
        return password;
	}
	
	public void follow(int follower, int followee) {
		logger.info("User "+ follower +" is following user "+ followee);	
		
		String insertFollowing = "INSERT INTO following(stalker_id, followee_id)"
				+" VALUES(:follower, :followee)";
        
        int count = getSimpleJdbcTemplate().update(insertFollowing,
            new MapSqlParameterSource().addValue("follower",follower)
                .addValue("followee", followee));
        
        logger.info("Rows affected: " + count);
	}

	public void unfollow(int follower, int followee) {
		logger.info("User "+ follower +" stopped following user "+ followee);
		
		String deleteFollowing = "DELETE FROM following"
				+" WHERE stalker_id = :follower AND followee_id = :followee";
		
		int count = getSimpleJdbcTemplate().update(deleteFollowing, new MapSqlParameterSource()
				.addValue("follower", follower)
				.addValue("followee", followee));
        
        logger.info("Rows affected: " + count);
	}

	
	/**
	 * Object Mappers
	 */
	
	private static class UserMapper implements ParameterizedRowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        	User user = new User();
        	user.setId(rs.getInt("id"));
        	user.setName(rs.getString("name"));
        	user.setUsername(rs.getString("nickname"));
        	user.setDescription(rs.getString("description"));

            return user;
        }
    }
	
	private static class FollowingMapper implements ParameterizedRowMapper<Integer> {

        public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
        	return rs.getInt("followee_id");
        }
    }
	
	private static class PasswordMapper implements ParameterizedRowMapper<String> {

        public String mapRow(ResultSet rs, int rowNum) throws SQLException {
        	return rs.getString("password");
        }
    }
}
