package pt.dcruz.tweets.dao;

import java.util.List;
import pt.dcruz.tweets.model.User;

public interface UserDao {

    public List<User> getUserList();

    public List<User> searchUsers(String query);
    
    public User getUser(int id);
    
    public User getUser(String username);
    
    /**
     * Save a new user on the database
     * @param user
     * @return The user ID
     */
    public int saveUser(User user);
    
    public String getUserPassword(int id);

    public List<Integer> getFollowingList(int user);

    public void follow(int follower,int followee);

    public void unfollow(int follower,int followee);
}

