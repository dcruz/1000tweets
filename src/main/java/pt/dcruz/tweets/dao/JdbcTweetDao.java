package pt.dcruz.tweets.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcDaoSupport;

import pt.dcruz.tweets.model.Tweet;

public class JdbcTweetDao extends SimpleJdbcDaoSupport implements TweetDao {

	public static int DEFAULT_TIMELINE_SIZE = 20;
	
    /** Logger for this class and subclasses */
	private static final Logger logger = LoggerFactory.getLogger(JdbcTweetDao.class);

    public List<Tweet> getTweetList() {
    	return getTweetList(DEFAULT_TIMELINE_SIZE);
    }
    
    /*
     * (non-Javadoc)
     * @see pt.dcruz.tweets.dao.TweetDao#getTweetList(int)
     */
    public List<Tweet> getTweetList(int howMany) {
        logger.info("Requesting "+ howMany +" tweets");
        
        String selectLastTweets = "SELECT s.id, s.text, s.timestamp, s.author, u.name, u.nickname"
        		+" FROM statuses s, users u"
        		+" WHERE s.author = u.id"
        		+" ORDER BY s.timestamp DESC"
        	    +" LIMIT 0, ?";
        
        List<Tweet> tweets = getJdbcTemplate().query(selectLastTweets,
        		new TweetMapper(),
        		new Object[]{howMany});
        
        logger.info("Returning "+ tweets.size() +" Tweets");
        
        return tweets;
    }
    
    /*
     * (non-Javadoc)
     * @see pt.dcruz.tweets.dao.TweetDao#getUserTweetList(int)
     */
    public List<Tweet> getUserTweetList(int user) {
    	return getUserTweetList(user, DEFAULT_TIMELINE_SIZE);
    }
    
    /*
     * (non-Javadoc)
     * @see pt.dcruz.tweets.dao.TweetDao#getUserTweetList(int, int)
     */
    public List<Tweet> getUserTweetList(int user, int howMany) {
    	logger.info("Requesting "+ howMany +" tweets from "+ user);

    	String selectUserTweets = "SELECT DISTINCT(s.id), s.text, s.timestamp, s.author, u.name, u.nickname"
    			+" FROM statuses s, users u"
    			+" WHERE s.author = u.id AND u.id = ?"
    			+" ORDER BY s.timestamp DESC"
    			+" LIMIT 0, ?";
        		
        /*
        		"SELECT DISTINCT(s.id), s.text, s.timestamp, s.author, u.name, u.nickname"
        		+" FROM statuses s, users u, following f"
        		+" WHERE (s.author = f.followee_id and s.author = u.id and f.stalker_id = ?)"
        		+" OR (s.author = ? and s.author = u.id)"
        		+" ORDER BY s.timestamp DESC"
        		+" LIMIT 0, ?";
        */

        List<Tweet> tweets = getJdbcTemplate().query(selectUserTweets,
        		new TweetMapper(),
        		new Object[]{user, howMany});
        
        logger.info("Returning "+ tweets.size() +" Tweets");
        
        return tweets;
    }
    
    /**
     * 
     */
    public List<Tweet> getFollowTweetList(int user, int howMany) {
        logger.info("Requesting "+ howMany +" from "+ user +" followers");
        
        String selectFollowTweets = "SELECT DISTINCT(s.id), s.text, s.timestamp, s.author, u.name, u.nickname"
        		+" FROM statuses s, users u, following f "
        		+" WHERE s.author = f.followee_id and f.stalker_id = ? and u.id = s.author"
        		+" ORDER BY s.timestamp DESC"
        		+" LIMIT 0, ?"; 

        
        List<Tweet> tweets = getJdbcTemplate().query(selectFollowTweets,
        		new TweetMapper(),
        		new Object[]{user, howMany});
        
        logger.info("Returning "+ tweets.size() +" Tweets");
        
        return tweets;
    }
    
    /**
     * 
     */
    public List<Tweet> getNewerTweets(int id) {
    	logger.info("Requesting tweets newer than tweet with ID: "+ id);
    	
    	String selectLastTweets = "SELECT new.id, new.text, new.timestamp, new.author, u.name, u.nickname "
    			+" FROM statuses new, users u"
    			+" WHERE new.timestamp > (SELECT old.timestamp FROM statuses old WHERE id = ?)"
    			+" AND new.author = u.id"
        		+" ORDER BY new.timestamp DESC";
    	
        List<Tweet> tweets = getJdbcTemplate().query(selectLastTweets,
        		new TweetMapper(),
        		new Object[]{id});
        
        logger.info("Received "+ tweets.size() +" newer than "+ id);
    	
    	return tweets;
    }
    
    /**
     * 
     */
    public List<Tweet> getOlderTweet(int id, int howMany) {
    	logger.info("Requesting "+ howMany +" tweets older than tweet with ID: "+ id);
    	
    	String selectOlderTweets = "SELECT old.id, old.text, old.timestamp, old.author, u.name, u.nickname"
    			+" FROM statuses old, users u"
    			+" WHERE old.timestamp < (SELECT new.timestamp FROM statuses new WHERE id = ?)"
    			+" AND old.author = u.id"
        		+" ORDER BY old.timestamp DESC"
    			+" LIMIT 0, ?";
    	
    	List<Tweet> tweets = getJdbcTemplate().query(selectOlderTweets,
        		new TweetMapper(),
        		new Object[]{id, howMany});
    	
        logger.info("Returning "+ tweets.size() +" Tweets");
    	
    	return tweets;
    }

    /**
     * 
     */
    public void saveTweet(Tweet tweet) {
        logger.info("Saving tweet: " + tweet.getText());
        
        int count = getSimpleJdbcTemplate().update(
            "insert into statuses(text, timestamp, author) values (:text, :timestamp, :author)",
            new MapSqlParameterSource().addValue("text", tweet.getText())
                .addValue("timestamp", tweet.getTimestamp())
                .addValue("author", tweet.getAuthorId()));
        logger.info("Rows affected: " + count);
    }
    
    /**
     * The Tweet Mapper
     */
    private static class TweetMapper implements ParameterizedRowMapper<Tweet> {

        public Tweet mapRow(ResultSet rs, int rowNum) throws SQLException {
            Tweet tweet = new Tweet();
            tweet.setId(rs.getInt("id"));
            tweet.setText(rs.getString("text"));
            tweet.setTimestamp(rs.getTimestamp("timestamp"));
            tweet.setAuthorId(rs.getInt("author"));
            tweet.setName(rs.getString("name"));
            tweet.setUsername(rs.getString("nickname"));
            
            return tweet;
        }

    }

}

