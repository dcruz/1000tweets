package pt.dcruz.tweets;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pt.dcruz.tweets.model.User;
import pt.dcruz.tweets.model.service.TweetManager;
import pt.dcruz.tweets.model.service.UserManager;

/**
 * Handles requests for the application home page.
 */
@RequestMapping @Controller
public class UserController {
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private UserManager userManager;
	private TweetManager tweetManager;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String searchUsers(@RequestParam("query") String query, Model model, HttpSession session) {
		
		model.addAttribute("users", userManager.searchUsers(query));
		if (session.getAttribute("loggedUser") != null) {
			User user = (User)session.getAttribute("loggedUser");
			model.addAttribute("following", userManager.getFollowingList( user.getId() ));
		}
		model.addAttribute("query", query);
		
		return "users";
	}
	
	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public String getUser(@PathVariable("id") int id, Model model, HttpSession session) {
		
		model.addAttribute("user", userManager.getUser(id));
		if (session.getAttribute("loggedUser") != null) {
			User user = (User)session.getAttribute("loggedUser");
			model.addAttribute("following", userManager.getFollowingList( user.getId() ));
			model.addAttribute("currentUser", userManager.getUser(id));
		}
		model.addAttribute("tweets", tweetManager.getUserTweetList(id));
		
		return "user";
	}

	@RequestMapping(value = "/users/{follower}/follow/{followee}", method = RequestMethod.GET)
	public String follow(@PathVariable("follower") int follower, @PathVariable("followee") int followee,
			Model model) {
		userManager.follow(follower, followee);
		return "redirect:/";
	}

	@RequestMapping(value = "/users/{follower}/unfollow/{followee}", method = RequestMethod.GET)
	public String unfollow(@PathVariable("follower") int follower, @PathVariable("followee") int followee,
			Model model) {
		userManager.unfollow(follower, followee);
		return "redirect:/";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model) {
		return "login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String doLogin(@RequestParam("username") String username, @RequestParam("password") String password,
			Model model, HttpSession session) {
		
		if (username.equals(null) || username.length() <= 0) {
			model.addAttribute("error_username", "username cannot be empty");
			return login(model);
		}
		
		if (password.equals(null) || password.length() <= 0) {
			model.addAttribute("error_password", "password cannot be empty");
			return login(model);
		}
		
		if ( !userManager.userExists(username)) {
			model.addAttribute("error_username", "username don't exist");
			return login(model);
		}
		
		User user = userManager.getUser(username);
		
		if ( !userManager.isPasswordValid(user.getId(), password)) {
			model.addAttribute("error_password", "passwords don't match");
			return login(model);
		}
		
		session.setAttribute("loggedUser", user);
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		return "signup";
	}
	
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String doSignup(@RequestParam("username") String username,
			@RequestParam("name") String name,
			@RequestParam("password") String password,
			@RequestParam("confirm-password") String confirmPassword,
			@RequestParam("description") String description,
			Model model, HttpSession session) {
		
		boolean hasError = false;
		
		if (username.equals(null) || username.length() <= 0) {
			logger.info("Username cannot be empty");
			model.addAttribute("error_username", "cannot be empty");
			hasError = true;
		}
		
		if (name.equals(null) || name.length() <= 0) {
			logger.info("Name cannot be empty");
			model.addAttribute("error_name", "cannot be empty");
			hasError = true;
		}
		
		if (password.equals(null) || password.length() <= 0) {
			logger.info("Password cannot be empty");
			model.addAttribute("error_password", "cannot be empty");
			hasError = true;
		} else if (!password.equals(confirmPassword)) {
			logger.info("Password and Password confirmation don't match");
			model.addAttribute("error_confirm_password", "passwords don't match");
			hasError = true;
		}
		
		if (hasError) {
			return "/signup";
		}
		
		User user = new User();
		user.setUsername(username);
		user.setName(name);
		user.setDescription(description);
		user.setPassword(password);
		
		int newUserId = userManager.addNewUser(user);
		
		logger.info("new user id: "+ newUserId);
		user = userManager.getUser(newUserId);
		
		session.setAttribute("loggedUser", user);
		
		return "redirect:/";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(Model model, HttpSession session) {
		session.removeAttribute("loggedUser");
		return "redirect:/";
	}
	
	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public TweetManager getTweetManager() {
		return tweetManager;
	}

	public void setTweetManager(TweetManager tweetManager) {
		this.tweetManager = tweetManager;
	}
	
}
