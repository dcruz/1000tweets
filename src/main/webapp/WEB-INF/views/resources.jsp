	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="icon" type="image/png" href="<c:url value="/resources/img/1000tweets-icon.png" />" />
	
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="<c:url value="/resources/css/ie.css"/>" type="text/css" media="screen" /><![endif]-->

	<!-- The 1140px Grid - http://cssgrid.net/ -->
	<link rel="stylesheet" href="<c:url value="/resources/css/1140.css"/>" type="text/css" media="screen" />
	
	<!-- Your styles -->
	<link rel="stylesheet" href="<c:url value="/resources/css/styles.css"/>" type="text/css" media="screen" />
	
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.7.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.17.custom.min.js"/>"></script>

	<!--css3-mediaqueries-js - http://code.google.com/p/css3-mediaqueries-js/ - Enables media queries in some unsupported browsers-->
	<script type="text/javascript" src="<c:url value="/resources/js/css3-mediaqueries.js"/>"></script>

    <script type="text/javascript" src="<c:url value="/resources/js/1000tweets.js"/>"></script>
    <link href='http://fonts.googleapis.com/css?family=Arvo' rel='stylesheet' type='text/css'>