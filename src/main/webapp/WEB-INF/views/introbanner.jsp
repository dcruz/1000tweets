	<div class="row banner-container">
		<div class="eightcol banner">
			<div class="slogan">
				<h2>Share some thoughts</h2> or follow someone you care about using <strong>1000tweets</strong>.
			</div>
			<div class="action-links">
				<a href="<c:url value="/signup" />">Signup</a> to start using or 
				<a href="<c:url value="/login" />">Login</a> let your voice be heard.
			</div>
		</div>
	</div>