		<div class="row">
			<div class="return-notice ninecol">
				<c:choose>
					<c:when test="${not empty loggedUser}">
						<a href="<c:url value="/" />">&larr; Return to your timeline</a>
					</c:when>
					<c:otherwise>
						<a href="<c:url value="/" />">&larr; Return to the main page</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>