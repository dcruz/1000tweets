            <%@include file="include.jsp" %>
            <c:choose>
            <c:when test="${not empty tweets}">
	            <c:forEach items="${tweets}" var="tweet">
	                <div class="tweets" id="tweet-${tweet.id}">      
						<img src="<c:url value='/resources/img/shadow.png' />" alt="" style="float: left;" height="48" width="48" />
						<div class="tweet-header">
						    <a href="<c:url value="/users/${tweet.authorId}"/>" class="name">${tweet.name}</a>
						    <span class="nickname">${tweet.username}</span>
						    <%-- TODO: implement link to single tweet --%>
						    <a href="tweet/${tweet.id}" title="<fmt:formatDate value='${tweet.timestamp}' type='both' timeStyle='short' dateStyle='medium'/>" class="timestamp"><fmt:formatDate value="${tweet.timestamp}" type="date"/></a>
						</div>
	                    <div class="tweet-body">
	                        <div class="tweet-text">
	                        	${tweet.text}
	                        </div>
	                    </div>
	                </div>
	 			</c:forEach>
			</c:when>
			<c:otherwise>
				<div class="empty-timeline">
					<p>Still no tweet. Share some thought to fill your timeline.</p>
				</div>
			</c:otherwise>
 			</c:choose>