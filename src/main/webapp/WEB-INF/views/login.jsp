<%@include file="include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>1000eyes: the twitter clone</title>

	<%@include file="resources.jsp" %>
</head>

<body>

	<%@include file="header.jsp" %>

	<%--
    <div class="container main">

		<div class="row">
            <div class="login-header ninecol">
            	<h2>Welcome back</h2>
            </div>
        </div>
			<div class="login">
				<form action="<c:url value="/login" />" method="post">
					<div class="row">
						<div class="twocol">
							<label for="login">Your username:</label>
						</div>
						<div class="fivecol last">
							<input name="login" type="text" class="" />
						</div>
					</div>
					<div class="row">
						<div class="twocol">
							<label for="password">Your password:</label>
						</div>
						<div class="fivecol last">
							<input name="password" type="password" />
						</div>
					</div>
					<div class="row" style="text-align: right">
						<div class="sevencol">
							<input type="submit" style="text-align: right" />
						</div>
					</div>
					<div class="row">
						<input type="text" />
					</div>
				</form>
			</div>
    </div>
    --%>
    
    <div class="container">
    	<div class="login-container">
    		<div class="login">
    			<div class="login-header">
    				<h2>Welcome back!</h2>
    				<p>Login to share some thoughts or check what the persons you follow have said.</p>
    			</div>

    			<form action="<c:url value="/login"/>" method="POST">
    				<div class="login-username clear">
    					<label for="username">Username</label>
    					<input type="text" name="username" id="username" />
    					<span class="error">${error_username}</span>
    				</div>
    				<div class="login-password clear">
    					<label for="password">Password:</label> 	
    					<input type="password" name="password" id="password" />
    					<span class="error">${error_password}</span>
    				</div>
    				<div class="login-submit clear">
    					<input type="submit" value="Login" />
    				</div>
    			</form>

    		</div>
    	</div>
    </div>

</body>

</html>
