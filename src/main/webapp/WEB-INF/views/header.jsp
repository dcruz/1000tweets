    <div class="container header">
        <div class="row">
            <h1 class="eightcol"><a href="<c:url value="/"/>">1000tweets</a></h1>
            <div class="account-area fourcol last">
                <div class="account-area-links">	
                <c:choose>
                	<c:when test="${not empty loggedUser}">
                		<a href="<c:url value="/users/${loggedUser.id}"/>" class="loggedUser" id="user-${loggedUser.id}"><c:out value="${loggedUser.name}" /></a> | <a href="<c:url value="/logout" />">logout</a>
                	</c:when>
                	<c:otherwise>
                    	<a href="<c:url value="/signup" />">Sign up</a> | <a href="<c:url value="/login" />">Log in</a>
                    </c:otherwise>
				</c:choose>
                </div>
            </div>
        </div>
    </div>