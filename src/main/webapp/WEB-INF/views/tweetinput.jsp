        <div class="row new-tweet">
            <div class="ninecol new-tweet-area">
            <form id="new-tweet-form" action="<c:url value="/add" />" method="POST">
                <textarea id="new-tweet-input" class="textarea-enlarged" name="text" placeholder="Add a new tweet..."></textarea>
                <div class="new-tweet-submit-panel" id="new-tweet-submit-panel">
                	<span class="new-tweet-error error"></span>
                    <span id="typed" class="hidden"><em id="keys-typed" class="keys-typed">140</em> characters left</span>
                    <input type="submit">
                </div>
            </form>
            </div>
        </div>
