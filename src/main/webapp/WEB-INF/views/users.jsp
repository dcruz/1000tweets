<%@include file="include.jsp" %>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Searching for users - 1000 tweets</title>

	<%@include file="resources.jsp" %>
</head>

<body>

	<%@include file="header.jsp" %>

    <div class="container main">

		<%@include file="returnNotice.jsp" %>

		<div class="row">
            <div class="userlist-header ninecol">
                <div>
                    <h2>searched for: <span class="query">${query}</span></h2>
                </div>
            </div>
        </div>
		<div class="row">
			<div class="userlist ninecol">
			<c:forEach items="${users}" var="user">
                <div class="user" id="user-${user.id}">
                	<div class="user-info">    
						<img src="resources/img/shadow.png" alt="" style="float: left;" height="48" width="48" />
						<div class="user-result-header">
						    <a href="users/${user.id}" class="name">${user.name}</a>
						    <span class="nickname">${user.username}</span>
						</div>
	                    <div class="user-result-body">
	                        ${user.description}
	                    </div>
                    </div>
                    <div class="user-following">
                    <c:if test="${not empty loggedUser}">
                    	<c:set var="contains" value="false" />
						<c:forEach var="item" items="${following}">
							<c:if test="${item eq user.id}">
								<c:set var="contains" value="true" />
							</c:if>
						</c:forEach>
	                    <c:choose>
	            		<c:when test="${contains}">
	            			<div class="user-following-block unfollow" id="user-${user.id}">
	            				<a href="<c:url value="/users/${loggedUser.id}/unfollow/${user.id}"/>">unfollow</a>
	            			</div>
	            		</c:when>
	            		<c:otherwise>
	            			<div class="user-following-block follow" id="user-${user.id}">
	            				<a href="<c:url value="/users/${loggedUser.id}/follow/${user.id}"/>">follow</a>
	            			</div>
	            		</c:otherwise>
	            		</c:choose>
	            	</c:if>
                    </div>
                </div>
 			</c:forEach>
 			</div>
 			
 			<%@include file="sidebar.jsp" %>
		</div>
    </div>

</body>

</html>
