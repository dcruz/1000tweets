            <div class="threecol last secundary-features">
                <div class="search-users">
                    <h2>Search users</h2>
                    <form action="<c:url value="/users" />" name="searchusers"  method="GET">
                    	<input type="search" name="query" value="${query}" placeholder="Search users..." />
                    	<input type="submit" value="search"/>
                    </form>
                </div>
                <div>
                    <h2>Help</h2>
                    <ul>
                        <li><a href="#">entry 1</a></li>
                        <li><a href="#">entry 2</a></li>
                        <li><a href="#">entry 3</a></li>
                    </ul>
                </div>
                <div class="copyright">
                    &copy; dcruz &mdash; 2012
                </div>
            </div>