        <%@include file="returnNotice.jsp" %>
        
        <div class="row">
            <div class="profile ninecol">
            	<div class="profile-info">
	            	<h1 class="nickname">${user.username}</h1>  
					<img src="<c:url value="/resources/img/shadow.png" />" alt="" style="float: left;" height="48" width="48" />
					<div class="profile-header">
					    <span class="name">${user.name}</span>
					</div>
					<div class="profile-body">
	                	${user.description}
	                </div>
                </div>
                <c:if test="${not empty loggedUser}">
                <div class="profile-following">
                	<c:set var="contains" value="false" />
					<c:forEach var="item" items="${following}">
						<c:if test="${item eq user.id}">
							<c:set var="contains" value="true" />
						</c:if>
					</c:forEach>
            		<c:choose>
            		<c:when test="${loggedUser.id eq currentUser.id}">
            			<div class="profile-self">
            				You!
            			</div>
            		</c:when>
            		<c:when test="${contains}">
						<div class="profile-following-block unfollow" id="user-${user.id}">
            				<a href="<c:url value="/users/${loggedUser.id}/unfollow/${user.id}" />">unfollow</a>
            			</div>
            		</c:when>
            		<c:otherwise>
            			<div class="profile-following-block follow" id="user-${user.id}">
            				<a href="<c:url value="/users/${loggedUser.id}/follow/${user.id}"/>">follow</a>
            			</div>
            		</c:otherwise>
            		</c:choose>
            	</div>
            	</c:if>
            </div>
        </div>
        
        <%--
        <div class="profile ninecol">
                <div class="user-info" style="display: inline-block;">
            	<h1 class="nickname">Spock</h1>  
				<img height="48" width="48" src="/1000tweets/resources/img/placeholder.png" alt="" style="float: left;">
				<div class="profile-header">
				    <span class="name">Mister Spock</span>
				</div>
				<div class="profile-body">
                	Polymath by nature, I do not understand all this fuss about feelings
                                </div>
                </div>
                <div style="float: right; background-color: lime; border-radius: 4px 4px 4px 4px; border: 1px solid green; text-align: center;" class="profile-follow">
<div style="text-align: left; padding: 0.4em 1.2em;">                       
<a style="display: block; width: 100%; color: white; text-decoration: none; text-align: center; height: 100%;" href="follow">follow</a>
</div>
                </div>
            </div>
            --%>