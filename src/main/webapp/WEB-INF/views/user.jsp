<%@include file="include.jsp" %>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>1000eyes: the twitter clone</title>

	<%@include file="resources.jsp" %>
</head>

<body>

	<%@include file="header.jsp" %>

    <div class="container main">
    	<%@include file="profile.jsp" %>

		<div class="row">
            <div class="timeline-header ninecol">
                <div>
                    <h2>Timeline of <span class="username">${currentUser.username}</span></h2>
                    <a href="<c:url value="." />" class="refresh" id="refresh">Refresh timeline</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="ninecol timeline">

            	<%@include file="tweetlist.jsp" %>

                <div class="more">
                    <a href="<c:url value="/extendTimeline">
                    	<c:param name="id" value="${loggedUser.id}" />
                    	<c:param name="count" value="${fn:length(tweets) + 10}" /></c:url>">Get more tweets</a>
                </div>
            </div>  
            <%@include file="sidebar.jsp" %>
            
        </div>
    </div>

</body>

</html>
