<%@include file="include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Sign up in 1000eyes</title>

	<%@include file="resources.jsp" %>
</head>

<body>

	<%@include file="header.jsp" %>
    
    <div class="container">
    	<div class="signup-container">
    		<div class="signup">
    			<div class="signup-header">
    				<h2>Welcome!</h2>
    				<p><em>Sign up</em> and start seeing and being seen by a 1000 eyes.</p>
    			</div>

    			<form action="<c:url value="/signup"/>" method="POST">
    			<fieldset>
    				<div class="signup-username clear">
    					<label for="username">Username</label>
    					<input type="text" name="username" id="username" />
    					<span class="error">${error_username}</span>
    				</div>
    				<div class="signup-name clear">
    					<label for="name">Name</label>
    					<input type="text" name="name" id="name" />
    					<span class="error">${error_name}</span>
    				</div>
    				<div class="signup-password clear">
    					<label for="password">Password</label>
    					<input type="password" name="password" id="password" />
    					<span class="error">${error_password}</span>
    				</div>
    				<div class="signup-confirm-password clear">
    					<label for="confirm-password">Confirm Password</label>
    					<input type="password" name="confirm-password" id="confirm-password" />
    					<span class="error">${error_confirm_password}</span>
    				</div>
    				<div class="signup-description clear">
    					<label for="description">Description<span class="optional"> (optional)</span></label>	
    					<textarea name="description" id="description"></textarea>
    				</div>
    				<div class="signup-submit clear">
    					<input type="submit" value="Signup" />
    				</div>
    			</fieldset>
    			</form>
    		</div>
    	</div>
    </div>

</body>

</html>
