$(document).ready(function(){
	var autoUpdateDelay = 60 * 1000;
	
	
    $("#refresh").click(function(event){
    	refresh()
        event.preventDefault();
    });

    $(".more a").click(function(event) {
    	console.log("Adding old tweets");
    	var tweetId = $(".tweets").last().attr("id").substring(6);
    	
        $.ajax({
            url: "more?id="+ tweetId,
            context: $(".more"),
            success: function (data) {
                if ( ! $(data).hasClass("empty-timeline")) {
                    $(this).before(data);
                } else {
                	$(".more").hide();
                }
            }
        });
        event.preventDefault();
    });

    $("#new-tweet-input").keyup(function(event) {
        var TWEET_LIMIT = 140;
        var keysLeft = TWEET_LIMIT - $("#new-tweet-input").val().length;

        $("#keys-typed").text(keysLeft);

        if (keysLeft < 0) {
            $("#new-tweet-input").addClass("input-error");
            $("#keys-typed").addClass("error");
        } else {
            $("#new-tweet-input").removeClass("input-error");
            $("#keys-typed").removeClass("error");
        }
    });

    // Collapse text input field on load
    $("#new-tweet-input").removeClass("textarea-enlarged");
    $("#new-tweet-submit-panel").addClass("hidden");
    $("#typed").removeClass("hidden");
    
    $("#new-tweet-input").focus(function(event) {
        $("#new-tweet-input").addClass("textarea-enlarged");
        $("#new-tweet-submit-panel").removeClass("hidden");
    });

    $("#new-tweet-input").focusout(function(event) {
        var newTweetInput = $("#new-tweet-input");

        if (newTweetInput.val().length === 0) {
            newTweetInput.removeClass("textarea-enlarged");
            $("#new-tweet-submit-panel").addClass("hidden");
        }
    });
    
    $("#new-tweet-form").submit(function(event) {
    	var id = $(this).attr("id").substring(4);
    	console.log("id: "+ id);
    	
		$.post("add", { text: $("#new-tweet-input").val() }, function () {
			refresh();
		}).success(function() {
			$("#new-tweet-input").val("");
		}) ;
		
		/* TODO: on Success AJAX refresh page */
		/* TODO: on Error, warn user */
		
    	event.preventDefault();
     });
    
    function refresh() {
    	console.log("Refreshing timeline");
    	var firstTweet = $(".tweets").first();
    	console.log("tweet id: "+ tweetId);
    	if(firstTweet.length > 0) {
    		var tweetId = firstTweet.attr("id").substring(6);
    		
    		$.ajax({
	            url: "newer?id="+ tweetId,
	            context: $(".timeline"),
	            success: function (data){
	                if ( ! $(data).hasClass("empty-timeline")) {
	                	$(this).prepend(data).effect("highlight", {}, 3000);
	                }
	            }
	        });
    	} else {
    		/* Get full list when adding first tweet */
    		var loggedUser = $(".loggedUser").first().attr("id").substring(6);
    		
    		$.ajax({
	            url: "timeline?id="+ loggedUser,
	            context: $(".timeline"),
	            success: function (data){
	            	$(this).prepend(data);
	            }
	        });
    	}
    }
    
    $(".user-following-block").click(function(event) {
    	var clicked = $(this);
    	var id = $(this).attr("id").substring(5);
    	
    	if(clicked.hasClass('follow')) {
    		followUser(id, clicked);
    	} else if(clicked.hasClass('unfollow')) {
    		unfollowUser(id, clicked);
    	}

    	event.preventDefault();
    });

    function followUser(id, element) {
    	var loggedUser = $(".loggedUser").attr("id").substring(5);
    	
        $.ajax({
            url: "users/"+ loggedUser +"/follow/"+ id,
            context: element,
            success: function (){
            	console.log("on follow: "+ element.val());
                element.removeClass("follow");
                element.addClass("unfollow");
                element.children().text("unfollow");
            }
        });
    }

    function unfollowUser(id, element) {
    	var loggedUser = $(".loggedUser").attr("id").substring(5);
    	
        $.ajax({
            url: "users/"+ loggedUser +"/unfollow/"+ id,
            context: element,
            success: function (){
            	console.log("on unfollow: "+ element.val());
                element.removeClass("unfollow");
                element.addClass("follow");
                element.children().text("follow");
            }
        });
    }
    
    function autoUpdate() {
    	var firstTweet = $(".tweets").first();
   		var tweetId = firstTweet.attr("id").substring(6);
		
		$.ajax({
            url: "newer?id="+ tweetId,
            timeout: 2000,
            context: $(".timeline"),
            success: function(data) {
                if ( ! $(data).hasClass("empty-timeline")) {
                	$(this).prepend(data).effect("highlight", {}, 3000);
                }
                window.setTimeout(autoUpdate, 10000)
            }
        });
    }
    autoUpdate();
    
});